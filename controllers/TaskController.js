const Task = require('../models/Task.js')

// #2 Create a controller function for retrieving a specific task
module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((task, error) => {
		if(error){
			return error
		}

		return task
	})

}

// #6 Create a controller function for changing the status of a task to complete
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			return error
		}

		result.name = newContent.name 

		return result.save().then((updatedTask, error) => {
			if(error){
				return error
			}

			return updatedTask
		})
	})
}

// --------------
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}