const express = require('express')
const TaskController = require('../controllers/TaskController.js')
const router = express.Router()

// #1 Create a route for getting a specific task
router.get('/tasks/:id', (request, response) => {
	TaskController.getTask(taskId).then((task, error) => response.send(task))
})

// #5 Create a route for changing the status of a task to complete
router.put('tasks/:id/complete', (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then((updatedTask) => response.send(updatedTask))
})




module.exports = router